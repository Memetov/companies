import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import { deleteDepartment } from '../../actions/departmentActions';

class DepartmentListItem extends Component {

    deleteDepartment(){
        if (confirm('Do you sure that you want to delete?'))
            this.props.deleteDepartment(this.props.department.id);
    }

    render() {

        const { props: { department } } = this;

        return (
            <tr>
                <td className="number">{department.id}</td>
                <td className="content">
                    <Link to={{ pathname: "view-department", query: { departmentId: department.id }}}>
                        {department.name}
                    </Link>
                </td>
                <td className="options">
                    <Link to={{ pathname: "edit-department", query: { departmentId: department.id }}}><i className="fa fa-pencil"></i></Link>&nbsp;
                    <span><i className="fa fa-trash" onClick={::this.deleteDepartment}></i></span>
                </td>
            </tr>
        );
    }
}

DepartmentListItem.propTypes = {
    deleteDepartment: React.PropTypes.func.isRequired
};

export default connect(null, { deleteDepartment })
(DepartmentListItem);
